package com.mysql.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.example.demo.model.Item;
import com.mysql.example.demo.repo.ItemRepository;

@RestController
public class ItemController {

	@Autowired
	private ItemRepository iRepo;
	
	@PostMapping("/add")
	public String addItem(@RequestBody Item obj) {
		iRepo.save(obj);
		return "Saved";
	}
	
	@GetMapping("/view")
	public Iterable<Item> viewItem(){
		Iterable<Item> check = iRepo.findAll();
		System.out.println(check);
		return iRepo.findAll();
		
	}
	
	
	
}
